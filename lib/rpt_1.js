

function retentionCalc(r){
  this.data2 = r[1]
  if(r[0] == 0) return 0;
  return Math.round(r[1]*1000 / r[0]) / 10 + '%';
}


module.exports =  {

  title : '留存率',
  columns : [
    {
      name : 'New users',
      queries : [{
        type : 'new',
      }],
    },

    {
      name : '1 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 1,
        }
      ], 
      calculate : retentionCalc,
    },

    {
      name : '2 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 2,
        }
      ], 
      calculate : retentionCalc,
    },

    {
      name : '3 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 3,
        }
      ], 
      calculate : retentionCalc,
    },

    {
      name : '4 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 4,
        }
      ], 
      calculate : retentionCalc,
    },

    {
      name : '5 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 5,
        }
      ], 
      calculate : retentionCalc,
    },

    {
      name : '6 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 6,
        }
      ], 
      calculate : retentionCalc,
    },

    {
      name : '7 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 7,
        }
      ], 
      calculate : retentionCalc,
    },



    {
      name : '14 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 14,
        }
      ], 
      calculate : retentionCalc,
    },


    {
      name : '30 period',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 30,
        }
      ], 
      calculate : retentionCalc,
    },


    /*
    {
      name : 'Active users',
      queries : [{
        type : 'active',
      }],
    },

    {
      name : 'Events',
      queries : [{
        type : 'event',
      }],
    },
    {
      name : 'active rate',

      queries: [
        {
          type : 'event', 
          evt : 'visit',
        }, 
        {
          type : 'active',
          evt  : 'visit',
        }
      ], 

      calculate : function(r){
        return r[0] / r[1];
      }
    }
    */


  ]
};
