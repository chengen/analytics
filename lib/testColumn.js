var Column = require('./column');

var df =  {
  name : '1 day retention',

  queries: [
  /*

    {
      type : 'active', 
    }, 
    */

    {
      type : 'new', 
    }, 

  /*
    {
      type : 'active',
      evt  : 'visit',
      offset : 5,
    }
    */
  ], 

  calculate : function(r){
    this.data2 = r[1];
    if(r[0] == 0) 
      return 0;
    else 
      return Math.round(r[1] * 1000 / r[0]) / 10 + '%';
  }
};


var opt = {
  periodType : 'day',
  start : '20160908',
  end : '20160908',
  //dimension : ['version']
};

var col = new Column(opt, df);

col.get(function(err, res){
  if(err) {
    return console.log('ERRROR', err);
  }
  console.log('isDone', col.isDone);
  console.log('RES', JSON.stringify(res, null, 4));
});



