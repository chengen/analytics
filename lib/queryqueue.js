var async = require('async');

var rclient = require('./redis');
var logger = require('./logger');
var MetaQuery = require('./metaquery');

function uniq(key, done) {
  rclient.hget('mqHash', key, function(err, res) {
    done(err, res);
  });
}

/*
 *When push a mq into cache, need to push a queue list and a queue hash
 *queue list for pop, queue hash for uniq
 *@param {string} mq MetaQuery.serialized
 *Pay attention: key in the hash, serialized in the list
 */

function push(mq, done) {

  console.log('PUSH', mq);

  /*
  if(!(mq instanceof MetaQuery)) {
    return done && done(mq + ' is not MetaQuery');
  }
  */

  var key = mq.key();
  var serialized = mq.serialize();

  uniq(key, function(err, res) {
    if (err) {
      logger.error(err);
      return done && done(err, res);
    }

    if (!res) {
      async.parallel([

          function(callback) {
            rclient.rpush('mqList', serialized, function(err, res) {
              callback(err, res);
            })
          },
          function(callback) {
            rclient.hset('mqHash', key, 1, function(err, res) {
              callback(err, res);
            })
          }

        ],
        function(err, results) {
          done && done(err, results);
        });
    } else {
      logger.debug('The mq is in the queue ' + serialized);
      done && done(err, res);
    }
  });

}

/**
 *First:pop one task(serialized) from the list
 *Second:change corresponding key's value to 2 in the hash
 *
 */
function pop(done) {
  rclient.lpop('mqList', function(err, res) {
    var mq ;
    if (res) {
      mq = MetaQuery.deserialize(res);
      var key = mq.key();
      rclient.hset('mqHash', key, 2, function(err, r) {
        if (err) {
          logger.debug(err);
        } else {
          logger.debug('<running> ' + res);
        }
      });
    }
    done(err, mq);
  });
}


/**
 * 
 */
function del(mq, done) {
  var key = mq;
  if(mq instanceof MetaQuery) {
    key = mq.key();
  }
  
  rclient.hdel('mqHash', key, function(err, res) {
    done && done(err, res);
  });
}

function handleInit() {
  rclient.hgetall('mqHash', function(err, res) {
    if (err) logger.error(err);
    if (res) {
      Object.keys(res).forEach(function(key) {
        if (res[key] == 2) {
          rclient.hdel('mqHash', key, function(err, res) {
            if (err) logger.error(err);
            logger.debug('[queryquery init success]');
          })
        }
      })
    }
  })
}

exports.push = push;
exports.pop = pop;
exports.del = del;
exports.handleInit = handleInit;
