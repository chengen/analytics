var async = require('async');
var period = require('./period');
var logger = require('./logger');
var MetaQuery = require('./metaquery');

var TAG = 'Query';

/*
 * Query query obj
 */
function Query(opt) {
  if (!opt)
    throw Error('Missing opt');
  this.opt = opt;
  this.metaQueries = [];
  this.split();
  this.isDone = false;
  this._result = [];
}

function deepCopy(obj){
  return JSON.parse(JSON.stringify(obj));
}

/*
 *One query will be splited 2 metaquery
 *type: click || conversion
 *dimension: dimension || null
 */
Query.prototype.split = function() {
  var self = this;

  var periods = period.range(self.opt.periodType, self.opt.start, self.opt.end);
  logger.debug(TAG, 'periods', periods);

  periods.forEach(function(p){
    //new users in this period.
    var mq = deepCopy(self.opt);
    mq.periods = [p];
    self.metaQueries.push(new MetaQuery(mq));
  });
}


/**
 * Get result
 */
Query.prototype.get = function(done) {
  var self = this;
  async.eachLimit(self.metaQueries, 1, function(mq, callback){
    mq.get(callback);
  }, function(err){
    if(err) {
      return done && done(err);
    }
    self.isDone = self.metaQueries.every(function(mq){
      return mq._result.status != 1;
    });

    self._result = self.metaQueries.map(function(mq){
      return mq._result;
    });

    done(null, self._result);
  });
}


Query.prototype.execute = function() {
  var self = this;
  var results = [];
  async.eachOfLimit(self.metaQueries, 1, function(mq, index, callback){
    console.log('index', index);
    var rep = self.results[Math.floor(index/2)];
    var repKey = index % 2 == 0 ? 'a' : 'b';
    mq.execute(function(err, res){
      rep[repKey] = res;
      console.log(err, res);
      callback(err, res);
    });
  }, function(err, res){
    self.results.forEach(function(r){
      if(r.a && r.b && r.a.length > 0 && r.b.length > 0) {
        r.data = r.b[0].count / r.a[0].count;
      }
    });
    console.log(self.results);
  })
}

module.exports = Query;
