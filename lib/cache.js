var async = require('async');

var rclient = require('./redis');
var logger = require('./logger');
var mongo = require('./mongo');


module.exports = {

  /*
   * @see getCache()
   */
  get : function get(key, done) {
    return this.getCache(key, done);
  },

  /*
   * set cache and mapping.
   *
   * @param {MetaQuery} mq Meta query to cache.
   * @param {Function} done callback.
   */
  set : function set(mq, done) {
    var self = this;
    async.parallel([
      function(callback){
        self.setCache(mq, function(err, res){
          return callback(err, res);
        });
      },
      /*
      function(callback){
        self.setMapping(mq, function(err, res){
          return callback(err, res);
        })
      },
      */
    ], function(err, res){
      done(err, res);
    });
  },

  /*
   * Get cache by key from redis.
   * @param key {String} key of the cache.
   */
  getCache : function getCache(key, done) {
    rclient.get(key, function(err, res) {
      //console.log(err, res);
      res = JSON.parse(res);
      //console.log('JSON RES', res);
      done(err, res);
    })
  },

  /*
   * set data into cache, and set the cache expire time
   * @param {MetaQuery} mq Meta query to cache.
   * @param {Function} done for cache
   */
  setCache : function setCache(mq, done) {
    var key = mq.key();
    var value = JSON.stringify(mq._results);
    var expires = mq.expires();

    logger.debug('key', key);
    logger.debug('value', value);
    logger.debug('expires', expires);


    rclient.set(key, value, function(err, res) {
      if (err) {
        logger.error(err);
      } else {
        if (expires >= 0) {
          rclient.expire(key, expires, function(err, res) {
            if (err) {
              logger.error(err);
            }
            logger.debug('Expire: ' + key + expires) ;
          })
        }
      }
      done(err, res);
    });
  },

  /*
   * Delete cache by key.
   * @param key {String} key of the cache.
   */
  deleteCache : function deleteCache (key, done) {
    rclient.del(key, function(err, res) {
      return done(err, res);
    });
  },

  /*
   * Persistant key query to mongodb.
   * {id : 'md5 of serialized key', start:"20121212", end : "20121213",...} 
   * So we can query find the keys by some condition.
   * 
   * @param mq {MetaQuery} serialized object.
   * @param done {Function} callback after saved.
   * 
   * @return {null}
   *
   */
  setMapping : function setMapping(mq, done){
    var obj = JSON.parse(mq.serialize());
    logger.debug('query', obj);
    obj._id = mq.key();

    mongo.getClient(function(err, client){
      if(err) {
        logger.error('<setMapping>', err.stack);
        return done(err);
      }

      client
        .db('ares')
        .collection('mapping')
        .save(obj, function(err, res){
          done(err, res);
        });
    });
  },

  /* 
   * Delete mapping by key
   */
  deleteMapping : function deleteMapping(key, done){
    mongo.getClient(function(err, client){
      if(err){
        logger.error('<deleteMapping>', err.stack);
        return done(err);
      }

      client
        .db('ares')
        .collection('mapping')
        .remove({_id : key}, function(err, res){
          done(err, res);
        });
    });
  },

  /*
   * Find cached query by cond.
   * @param query {Ojbect} mongodb style query object.
   */
  find : function find(query, done){
    mongo.getClient(function(err, client){
      if(err){
        logger.error('<find>', err.stack);
        return done(err);
      }
      
      client
        .db('ares')
        .collection('mapping')
        .find(query)
        .toArray(function(err, res){
          done(err, res);
        });
    });
  },

  /*
   * Drop cached query by cond.
   * @param query {Object} mongodb style query object.
   */
  drop : function drop(query, done){
    var self = this;

    self.find(query, function(err, res){
      if(err){
        return done(err);
      }

      var queue = async.queue(function(id, callback){
        /*
         * first delete the canche
         * then delete the mapping
         */
        async.series([
          function(cb){
            self.deleteCache(id, function(err, res){
              cb(err, res);
            });
          },

          function(cb){
            self.deleteMapping(id, function(err, res){
              cb(err, res);
            });
          }
        ], function(err, res){
          callback();
        });
      }, 10);

      queue.drain = function(){
        done();
      }

      res.forEach(function(obj){
        queue.push(obj._id);
      });
      
    });
  }

}

