var async = require('async');
var logger = require('./logger');
var Column = require('./column');
var period = require('./period');

var TAG = 'Report';

/*
 * Report obj
 * Report contains one or more columns
 */

function Report(opt, rpt) {
  if (!opt)
    throw Error('Missing opt');

  if (!rpt)
    throw Error('Missing report definition');

  this.opt = opt;
  this.rpt = rpt;
  this.columns = [];
  this.split();
  this.isDone = false;
  this.result = null;
  this.periods = period.range(this.opt.periodType, this.opt.start, this.opt.end);
}

function deepCopy(obj){
  return JSON.parse(JSON.stringify(obj));
}

/*
 *One query will be splited 2 metaquery
 *type: click || conversion
 *dimension: dimension || null
 */
Report.prototype.split = function() {
  var self = this;
  self.rpt.columns.forEach(function(cdf){
    console.log('CDF', cdf);
    var obj = deepCopy(self.opt);
    self.columns.push(new Column(obj, cdf));
  });
}


/**
 * Get result
 */
Report.prototype.get = function(done) {
  var self = this;
  async.eachLimit(self.columns, 1, function(column, callback){
    column.get(callback);
  }, function(err){
    if(err) {
      return done && done(err);
    }

    self.isDone = self.columns.every(function(q){
      return q.isDone;
    });

    self._result = self.columns.map(function(q){
      return q._result;
    });

    if(self.opt.dimension) {
      self.assembleDim();
    }else{
      self.assemblePeriods();
    }
    
    done(null, self._result);
  });
}


Report.prototype.assemblePeriods = function() {
  var self = this;
  var cols = self._result.length;
  self._result = self.periods.map(function(p, i){
    var row = [{
      status : 0,
      data : p
    }];
    for(var j=0; j<cols; j++) {
      row.push(self._result[j][i]);
    }
    return row;
  });
}

Report.prototype.assembleDim = function() {
  var self = this;
  var cols = self._result.length;

  var keys = self._result.reduce(function(keys, col){
    var colKeys = col.data.map(function(item){
      return item.key;
    });
    return keys.concat(colKeys);
  }, [])
  .sort()
  .reduce(function(keys, cur){
    if(keys.length == 0 || keys[keys.length-1] != cur) {
      keys.push(cur);
    }
    return keys;
  }, []);

  console.log('keys', keys);

  self._result = keys.map(function(key){
    var row = [key];

    for(var i=0; i<cols; i++){
      var list = self._result[i].data;
      //find key in that col
      var idx = list.findIndex(function(kk){
        return kk.key == key;
      });
      if(idx >= 0 ){
        row.push(list[idx].data);
      }else{
        row.push(0);
      }
    }

    console.log('ROW', row);

    return row;
  });
  
}




module.exports = Report;
