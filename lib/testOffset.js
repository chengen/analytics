var MetaQuery = require('./metaquery');




var mq = new MetaQuery({
  type : 'active',
  periods : ['20160902'],
  dimension:['country'],
});


var timer = setInterval(function(){
  console.log('checking.....');
  mq.get(function(res){
    if(res || mq.isFuture ) {
      clearInterval(timer);
      console.log('RESULT', res);
      process.exit(0);
    }
  });
}, 100);

