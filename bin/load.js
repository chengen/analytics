#!/usr/bin/env node

/*
 *
 *
 * Author: chengen@jccy-tech.com
 * 
 * Load log file (gzip or plain text) into mongodb.
 *
 *
 */

var fs = require('fs');
var zlib = require('zlib');
var parser = require('../lib/parser');
var Event = require('../lib/model_event');
var conn = require('../lib/conn');
var importer = require('../lib/importer');
var logger = require('../lib/logger');

var argv = require('optimist')
    .usage('Usage: $0 file')
    .alias('m', 'monitor')
    .argv;
 

var file = argv._[0];


if(!file || !fs.existsSync(file)) {
  console.error('File', file, 'not exists!');
  process.exit(1);
}


if(isGzip(file)) {
  gunzipFile(file, function(tmpFile){
    job(tmpFile, function(){
      fs.unlinkSync(tmpFile);
    });
  });
}else{
  job(file);
}

function isGzip(file){
  return file.endsWith('.gz') || file.endsWith('.gzip');
}

function job(file, done){
  var opt = {
    queueSize : 10000,
    concurrency : 20,
    monitor : argv.m,
  };
  logger.info('Start importing', file);
  importer(file, filter, task, opt, function(stats){
    logger.info('Import finished.');
    logger.info(JSON.stringify(stats, null, 4));
    done && done();
    process.exit(0);
  });
}


/**
 * event log with stutus code 200
 */
function filter(line){
  var is200 = line.indexOf(' 200 ') > 0;
  var isEvt = line.indexOf('evt=') > 0 || (line.indexOf('version=1.0.14') > 0 && line.indexOf('/config?') > 0);
  var isInj = line.indexOf('evt=inj') > 0;
  return !is200 || !isEvt || isInj;
}


/*
 * Process a line.
 */
function task(line, done){
  var log = parser(line);
  if(!log) return done();
  if(log.version == '1.0.14' && log.pathname == '/config') {
    log.evt = 'visit';
  }
  Event.save(log, done);
}


function gunzipFile(input, done){
  var tempFile  = 'tmp-' + new Date().getTime() + '.log';
  var inp = fs.createReadStream(input);
  var out = fs.createWriteStream(tempFile);
  var gunzip = zlib.createGunzip();

  logger.info('Unzipping file', input, 'to', tempFile);
  gunzip.on('end', function(){
    logger.info('Unzip finished.');
    done(tempFile);
  });
  inp.pipe(gunzip).pipe(out);
}


