var es = require('event-stream');
var fs = require('fs');
var async = require('async');
var zlib = require('zlib');


var argv = require('optimist')
    .usage('Usage: $0 file1  file2 ...')
    .alias('f', 'file')
    .describe('f', 'input file')
    .demand(['f'])
    .argv;



var inp = fs.createReadStream(argv.f);
var gunzip = zlib.createGunzip();

inp.pipe(gunzip).pipe(process.stdout);
