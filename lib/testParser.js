var parser = require('./parser');
var split2 = require('split2');
var split = require('split');
var through2 = require('through2');
var fs = require('fs');



var filterInj = through2.obj(function(line, encoding, callback){
  var is200 = line.indexOf(' 200 ') > 0;
  var isEvt = line.indexOf('evt=') > 0 || (line.indexOf('version=1.0.14') > 0 && line.indexOf('/config?') > 0);
  var isInj = line.indexOf('evt=inj') > 0;

  //if(is200 && isEvt && !isInj){
  //  this.push(line);
  //}
  if(line && line.length && line.trim()) {
    this.push(line);
  }
  callback();
});

var parse = through2.obj(function(trunk, encoding, callback){
  this.push(parser(trunk));
  callback();
});

var printJSON = through2.obj(function write(trunk, encoding, callback){
  this.push(JSON.stringify(trunk, null, 4) + '\n');
  callback();
});


process.stdin
  .pipe(split())
  //.pipe(filterInj)
  //.pipe(parse)
  //.pipe(printJSON)
  .pipe(fs.createWriteStream('o.log'));
  //.pipe(process.stdout);
