#!/usr/bin/env node

var async = require('async');

var logger = require('../lib/logger');
var cache = require('../lib/cache');
var qq = require('../lib/queryqueue');

var CONC = 4;


/**
 *For handling task interrupt
 *
 */

function init() {
  qq.handleInit(); 
}

init();

var q = async.queue(function(mq, callback) {
  console.time('mqTime');
  var key = mq.key();
  mq.execute(function(err, res) {
    qq.del(key);
    if (err) {
      logger.error(err);
      qq.push(mq);
    } else {
      console.timeEnd('mqTime');
    }
    callback();
  });
}, CONC);

q.drain = function() {
  logger.debug('excute queue is empty now~');
}

setInterval(function() {
  if (q.length() < 10) {
    qq.pop(function(err, mq) {
      if (err) return logger.error(err);
      if (mq) {
        q.push(mq);
      }
    })
  }
}, 100);


