var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./model_user');


var EventSchema = new Schema({
  //commont props
  logid   :{type : String, index:{unique:true}},  //event
  evt     :{type : String, index:true}, //event type
  uid     :{type : String, index:true}, //uniq user id
  time    :{type : Date, index:true},    //log time
  day     :{type : String, index:true},  //log date
  week    :{type : String, index:true},  //log week 
  month   :{type : String, index:true},  //log month
  val     :{type : String} //evt value
}, {shardKey: {date : 1}});


EventSchema.statics.save = function(obj, done){
  this.findOne({logid:obj.logid}, function(err, evt){
    if(err) return done(err);
    if(evt) return done(); //log imported
    var e = new Event(obj);
    e.save(function(err, evt){
      if(err) return done(err);
      if(obj.evt == 'visit') {
        User.save(obj, function(err, user){
          done(err);
        });
      }else{
        done();
      }
    });
  });
}


var Event = mongoose.model('Event', EventSchema);
module.exports =  Event;

