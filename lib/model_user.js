var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var verify = require('./verify');


var UserSchema = new Schema({
  //commont props
  uid     :{type : String, index:{unique: true}}, //uniq user id
  source  :{type : String, index:true},  //source
  subid   :{type : String, index:true},  //sub source id
  prod    :{type : String, index:true},  //product
  iversion :{type : String, index:true},  //install version
  version :{type : String, index:true},  //version
  rtime   :{type : Date, index:true},  //log time
  rday    :{type : String, index:true},  //log date
  rweek    :{type : String, index:true},  //log week 
  rmonth   :{type : String, index:true},  //log month
  ip      :{type : String, index:true},  //ip
  country :{type : String, index:true},  //country code
  lastVisit:{type : Date, index:true},  //country code
  lastInject:{type : Date, index:true},  //country code
  visitCount:{type : Number, index:true},  //country code
  lastVisitInterval:{type : Number, index:true},  //country code
  live    :{type : [String], index:true}, //live days.
  cheat   :{type : [String], index:true}, //is Fake.

  //visit log
  browserlist :{type : [String], index:true}, //browser list.
  seclist :{type : [String], index:true}, //security software list.
  defaultbrowser :{type : String, index:true}, //default browser
  uuid2   :{type : String, index:true}, //uuid2
  mjv     :{type : String, index:true}, //major version
  mnv     :{type : String, index:true}, //minor version
  osv     :{type : String, index:true}, //os version mjv.mnv
  buidn   :{type : String, index:true}, //build no
  arc     :{type : String, index:true}, //arc x86 or x64
  chassis :{type : String, index:true},
  uidcheck:{type : String, index:true},
  uuidcheck:{type : String, index:true},

});


UserSchema.statics.save = function(obj, done){
  this.findOne({$or : [{uid:obj.uid}, {uuid2:obj.uuid2}]}, function(err, _user){
    if(err) return done(err);
    if(_user){
      console.log('user found....');
    }
    if(_user && _user.uid == obj.uid) {
      //user already exists.
      //happens when log import order not right
      
      
      
      if(_user.rtime > obj.time) {
        _user.set('rtime', obj.time);
        _user.set('rday', obj.day);
        _user.set('rweek', obj.week);
        _user.set('rmonth', obj.month);
      }
      _user.set('lastVisit', obj.time);
      _user.set('visitCount', _user.visitCount+1);
      _user.set('lastVisitInterval', Math.round(_user.lastVisit.getTime()/1000 - _user.rtime.getTime() / 1000));
      //update live
      var live =  Math.round(_user.lastVisitInterval  / (3600 * 24));
      _user.set('live', live);

      _user.save(function(err, u){
        done(err, u, false);
      });
    }else{
      user = new User(obj);
      user.set('uuidcheck', _user ? 'repeat' : 'first');
      user.set('cheat', _user ? 'cheat' : 'normal');
      user.set('uidcheck', verify(obj.uid) ? 'good' : 'bad');
      user.set('iversion', obj.version);
      user.set('visitCount', 1);
      user.set('rtime', obj.time);
      user.set('rday', obj.day);
      user.set('rweek', obj.week);
      user.set('rmonth', obj.month);
      user.set('lastVisit', obj.time);
      user.set('lastVisitInterval', 0);
      user.set('live', '0day');

      user.save(function(err, u){
        done(err, u, true);
      });

    }
  });
};

var User = mongoose.model('User', UserSchema);

module.exports =  User;
