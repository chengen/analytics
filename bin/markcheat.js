#!/usr/bin/env node

/*
 *
 *
 * Author: chengen@jccy-tech.com
 * 
 * Mark cheat users.
 *
 *
 */



var verify = require('../lib/verify');
var User = require('../lib/model_user');
var async = require('async');
var conn = require('../lib/conn');
var mongo = require('../lib/mongo');

var queue = async.queue(function(user, callback){
  console.log(user._id, user.count);
  callback();
}, 10);

var isDone = false;
console.time('uidcheck');
queue.drain = function(){
  if(isDone) {
    console.log('All done!');
    console.timeEnd('uidcheck');
  }
}

mongo.getDb(function(err, db) {
  if (err) {
    return callback(err, null);
  }
  
  var agg = [];
  agg.push({
    $match : {uuid2:{$exists:true}}
  });

  agg.push({
    $group : {_id:'$uuid2', count : {$sum:1}}
  });

  agg.push({
    $match : {count:{$gt:1}}
  });

  db
    .collection('users')
    .aggregate(agg, function(err, res) {
      res.forEach(function(i){
        queue.push(i);
      });
    });

});


