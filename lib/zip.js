var es = require('event-stream');
var fs = require('fs');
var async = require('async');
var zlib = require('zlib');


var argv = require('optimist')
    .usage('Usage: $0 file1  file2 ...')
    .alias('f', 'file')
    .describe('f', 'output file')
    .demand(['f'])
    .argv;


var k = zlib.createGzip().pipe(fs.createWriteStream(argv.f));

process.stdin.pipe(k);
