var moment = require('moment');
var momentRange = require('moment-range');


var INP_FORMATS = ['YYYYMMDD', 'DDMMYYYY'];
var OUT_FORMATS = {
  day : 'YYYYMMDD',
  week : 'YYYYww',
  month : 'YYYYMM'
};

var ZONE_OFFSET = 480;

/*
 * Reture a range object from start to end.
 */
function getRange(start, end){
  var s = moment(start, INP_FORMATS).utcOffset(ZONE_OFFSET);
  var e = moment(end, INP_FORMATS).utcOffset(ZONE_OFFSET);
  return moment.range(s, e);
}

/*
 * Reture days,weeks,months of the range.
 * @params type  day/week/month
 */
function range(type, start, end){
  return getRange(start, end).toArray(type + 's').map(function(day){
    return day.format(OUT_FORMATS[type]);
  });
}


/**
 * Reture the next period.
 */
function offset(type, cur, _offset){
  return moment(cur, OUT_FORMATS[type])
          .add(_offset, type + 's').format(OUT_FORMATS[type]);
}


function isFuture(type, p, _offset){
  var cur = moment().utcOffset(ZONE_OFFSET);
  return Array.prototype.concat([], p).every(function(k){
    if(_offset) k = offset(type, k, _offset);
    var s = moment(k, OUT_FORMATS[type]).utcOffset(ZONE_OFFSET);
    return s.isAfter(cur, type);
  });
}

function isCurrent(type, p, _offset){
  var cur = moment().utcOffset(ZONE_OFFSET);
  return Array.prototype.concat([], p).some(function(k){
    if(_offset) k = offset(type, k, _offset);
    var s = moment(k, OUT_FORMATS[type]).utcOffset(ZONE_OFFSET);
    return s.isSame(cur, type);
  });
}

//console.log('isFuture:', isFuture('day', ['20160906', '20160907'], 1));
//console.log('isFuture:', isFuture('day', ['20160905', '20160904']));
//console.log('isFuture:', isFuture('day', ['20160806', '20160807']));
//
//console.log('isFuture:', isFuture('week', ['201638', '201639']));
//console.log('isFuture:', isFuture('month', '201610'));
//
//console.log('isCurrent:', isCurrent('day', ['20160906', '20160907']));
//console.log('isCurrent:', isCurrent('week', '201636'));
//console.log('isCurrent:', isCurrent('month', '201608'));



module.exports = {
  range : range,
  offset : offset,
  isFuture: isFuture,
  isCurrent: isCurrent
}


