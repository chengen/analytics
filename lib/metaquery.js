var crypto = require('crypto');
var querystring = require('querystring');
var async = require('async');
var moment = require('moment');
var period = require('./period');

var logger = require('./logger');
var mongo = require('./mongo');
var cache = require('./cache');
var qq = require('./queryqueue');


var TAG = 'MetaQuery';

/*
 * Constructor of MetaQuery.
 */
function MetaQuery(opt) {
  var self = this;

  if (!opt) {
    throw Error('invalid option: ' + opt);
  }


  self.type = opt.type; //new,active,event
  self.periodType = opt.periodType || 'day'; //day,week,month
  self.periods = opt.periods;
  self.evt = opt.evt || 'visit';
  self.offset = opt.offset;


  //sort the dimension
  self.dimension = opt.dimension;

  //sort filters keys.
  self.filters = {};
  if(opt.filters) {
    self.filters = Object.keys(opt.filters)
      .sort()
      .reduce(function(obj, key) {
        obj[key] = opt.filters[key];
        return obj;
      }, self.filters);
  }

  self.isFuture = period.isFuture(self.periodType, self.periods, self.offset);
  self.isCurrent = period.isCurrent(self.periodType, self.periods, self.offset);

  self.isDone = self.isFuture;
  //default result, running...
  self._result = {
    status : 1,
    current : self.isCurrent ? 1 : 0
  };

}

/**
 * Check if key in dimension or filter
 */
MetaQuery.prototype.contains = function(key) {
  var self = this;
  return (Array.isArray(self.dimension) && self.dimension.indexOf(key) >=0) || (key in self.filters);
}

/*
 * Get expires according to type and date.
 *  expires in 60 seconds by default.
 *@return {Integer} expiration in seconds.
 */
MetaQuery.prototype.expires = function() {
  var self = this;
  logger.debug(TAG, 'isFuture', self.isFuture);
  logger.debug(TAG, 'isCurrent', self.isCurrent);

  if(self.isFuture) return 0; //future cache none.
  if(self.isCurrent) return 3600; //current cache 60 senconds
  
  var isTemp = self.contains('live') || self.contains('cheat');
  if(isTemp) return 3600 * 2; //if contains 'live' or 'cheat', data expires in two hours.
  
  return -1; //permenantly
}


/*
 * Serialize MetaQuery instace in special key order. To make sure
 * different key order generate the same serilaized.
 *
 * @return {String} serialized MetaQuery instance.
 */
MetaQuery.prototype.serialize = function() {
  var self = this;

  if (!self._query) {
    self._query = {
      type: self.type,
      periodType: self.periodType,
      periods: self.periods,
      evt: self.evt,
      offset: self.offset,
      dimension: self.dimension,
      filters: self.filters,
    };
  }

  return JSON.stringify(self._query);
}


/* 
 * Create MetaQuery Ojbect from serialized string.
 *
 * @param serialized {String} serialized string created by serialized().
 * @return {MetaQuery} A MetaQuery instace.
 */
MetaQuery.deserialize = function(serialized) {
  var obj = JSON.parse(serialized);
  return new MetaQuery(obj);
}

/*
 * Key for this query.
 * @return {String} Md5 of the serialized query.
 */
MetaQuery.prototype.key = function() {
  if (!this._key) {
    this._key = 'mq:' + crypto.createHash('md5').update(this.serialize(), 'utf8').digest('hex');
  }
  return this._key;
}

MetaQuery.prototype.toString = function() {
  return this.serialize();
}

/*
 * Get results of this query.
 * 1. if query in cache, just return the query.
 * 2. if not, push the serialized obj to the Query Queue, and return unfinished results.
 *
 * @param done {Function} callback.
 * TODO after API is ready.
 */
MetaQuery.prototype.get = function(done) {
  var self = this;

  if(self.isFuture) {
    self._result.status = 2;
    return done && done(null, self._result);
  }

  var key = self.key();
  cache.get(key, function(err, res) {
    if (err) {
      logger.error(err);
      //qq.push(self);
      return done && done(err);
    } 

    if (res) {
      logger.debug('Cached: ' + key);
      logger.debug('Cached: ' + JSON.stringify(res));
      self._result.status = 0;
      self._result.data = res;
      self.isDone = true;
      done(null, self._result);
    } else {
      //put task in the queue
      qq.push(self);
      done(null, self._result);
    }
    
  });
}

/*
 *when get cache failed, return null;
 */
MetaQuery.prototype.getDefault = function() {
  return null;
}

/*
 * Generate group id for aggregation.
 * If no dimension, just return 'Summary',
 * else transform dimension array to object.
 * ['site', 'nation'] =>  {site : '$site', nation : '$nation'}
 */
MetaQuery.prototype.getGroupId = function(prefix) {
  if (!this.dimension || !Array.isArray(this.dimension)) {
    return null;
  } else {
    return this.dimension
      .reduce(function(obj, key) {
        if(prefix) {
          obj[key] = '$' + prefix + '.' + key;
        }else{
          obj[key] = '$' + key;
        }
        return obj;
      }, {});
  }
}


/*
 * Cache the result of this meta query.
 *
 */
MetaQuery.prototype.setCache = function(done) {
  var self = this;
  cache.set(this, function(err, res) {
    done && done(err, res);
  });
}

/*
 * Execute the MetaQuery according to the type.
 * @param done {Function} callback.
 */
MetaQuery.prototype.execute = function(done) {
  if (this.type === 'new') {
    this.countNew(done);
  } else if (this.type === 'active') {
    this.countActive(done);
  } else if (this.type === 'event') {
    this.countEvent(done);
  }
}





/*
 *create query obj support not
 *
 */
function createQueryObj(filters) {
  var obj = JSON.parse(JSON.stringify(filters));

  Object.keys(obj).forEach(function(key) {
    if (String(obj[key]).indexOf('(!)') !== -1) {
      obj[key] = {
        $ne: obj[key].slice(3) == 'null' ? null : obj[key].slice(3),
      };
    }
  })

  return obj;
}

/*
 * County new users.
 *
 * @param done {Function} callback.
 */
MetaQuery.prototype.countNew = function(done) {
  var self = this;

  var query = createQueryObj(self.filters);
  var periodKey = {day:'rday', week:'rweek', month:'rmonth'}[self.periodType];
  query[periodKey] = {
    $in: self.periods, 
  };


  logger.debug('QUERY:', query);

  var aggregation = [{
    $match: query
  }, {
    $group: {
      _id:self.getGroupId(), 
      count: {$sum:1}
    }
  }];

  logger.debug('AGG:', JSON.stringify(aggregation));

  mongo.getDb(function(err, db) {
    if (err) {
      return callback(err, null);
    }

    var _results = null;

    db
      .collection('users')
      .aggregate(aggregation, function(err, res) {
        process.nextTick(function() {
          done(err, res);
        });

        if (!err) {
          self._results = res;
          self.setCache();
        }
      });
  });
}

function deepCopy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

/*
 * Count active users.
 *
 * @param done {Function} callback.
 */
MetaQuery.prototype.countActive = function(done) {
  var self = this;
  var filters = self.filters;
  var periods = self.periods;

  /*
   * use offset only for retention calculation.
   * add first period rday/rweek/rmonth to filters.
   */
  if(self.offset) {
    logger.debug('has offset..............');
    filters = deepCopy(self.filters);
    periods = deepCopy(self.periods);
    
    //offset all periods
    periods = periods.map(function(p){
      return period.offset(self.periodType, p, self.offset);
    });
    logger.debug('offset periods:', periods);

    filters['r' + self.periodType] = self.periods[0];
    logger.debug('offset filters:', filters);
  }

  var query = {}; //createQueryObj(self.filters);
  var periodKey = {day:'day', week:'week', month:'month'}[self.periodType];
  query[periodKey] = {
    $in: periods, 
  };

  query.evt = self.evt;

  logger.debug('QUERY:', query);

  var filters = Object.keys(filters).reduce(function(f, key){
    f['props.' + key] = filters[key];
    return f;
  }, {});

  logger.debug('FILTERS:', filters);


  var aggregation = [
    {$match: query}, 
    {$group : { _id : '$uid'}},
    {$lookup : {from:'users', localField:'_id', foreignField:'uid', as:'props'}},
    {$unwind:'$props'},
    {$match:filters},
    {$group: { _id:self.getGroupId('props'), count: {$sum:1}}
  }];

  logger.debug('AGG:', JSON.stringify(aggregation));

  mongo.getDb(function(err, db) {
    if (err) {
      return callback(err, null);
    }

    var _results = null;

    db
      .collection('events')
      .aggregate(aggregation, function(err, res) {
        process.nextTick(function() {
          done(err, res);
        });

        if (!err) {
          self._results = res;
          self.setCache();
        }
      });
  });
}


/*
 * Count events.
 *
 * @param done {Function} callback.
 */
MetaQuery.prototype.countEvent = function(done) {
  var self = this;

  var query = {}; //createQueryObj(self.filters);
  var periodKey = {day:'day', week:'week', month:'month'}[self.periodType];
  query[periodKey] = {
    $in: self.periods, 
  };
  query.evt = self.evt;

  logger.debug('QUERY:', query);

  var filters = Object.keys(self.filters).reduce(function(f, key){
    //console.log('xxxx', f);
    f['props.' + key] = self.filters[key];
    return f;
  }, {});

  logger.debug('FILTERS:', filters);

  var aggregation = [
    {$match: query}, 
    {$group : { _id : '$uid', count:{$sum:1}}},
    {$lookup : {from:'users', localField:'_id', foreignField:'uid', as:'props'}},
    {$unwind:'$props'},
    {$match:filters},
    {$group: { _id:self.getGroupId('props'), count: {$sum:"$count"}}}
  ];

  logger.debug('AGG:', JSON.stringify(aggregation));

  mongo.getDb(function(err, db) {
    if (err) {
      return callback(err, null);
    }

    var _results = null;

    db
      .collection('events')
      .aggregate(aggregation, function(err, res) {
        process.nextTick(function() {
          done(err, res);
        });

        if (!err) {
          self._results = res;
          self.setCache();
        }
      });
  });
}



module.exports = MetaQuery;
