var logParser = require('nginx-log-parser');
var moment = require('moment');
var url = require('url');
var crypto = require('crypto');

//Nginx log parser


var log_format =  '$remote_addr - $remote_user [$time_local] $geoip_country_code "$request" ' + 
                  '$status $body_bytes_sent  "$request_body" "$http_referer" ' + 
                  '"$http_user_agent" "$http_x_forwarded_for"';

var time_format = 'DD/MMMM/YYYY:HH:mm:ss Z';


var parser = logParser(log_format);

function parseRequest(link){
  var i = link.indexOf('&url=');
  var inj_url = null;
  var res = {};
  if(i>=0) {
    res.injUrl= link.substring(i+5, link.length);
    if(res.injUrl) {
      res.injHost = url.parse('http://' + res.injUrl).hostname;
    }
    link = link.substring(0, i);
  }
  var obj = url.parse(link, true);
  obj.query.pathname = obj.pathname;
  return Object.assign({}, obj.query, res);
}

function parseLine(line) {
  var log = parser(line);

  if(log.status != '200' || !log.request) return null;

  //request
  var req = log.request.split(' ')[1];
  var obj = parseRequest(req);  

  obj.logid = crypto.createHash('md5').update(line, 'utf8').digest('hex');

  //date time utc+8
  var m = moment(log.time_local, time_format);

  obj.time = m.toDate();
  obj.day = m.utcOffset(480).format('YYYYMMDD');
  obj.week = m.utcOffset(480).format('YYYYww');
  obj.month = m.utcOffset(480).format('YYYYMM');


  //source
  if(obj.source && obj.source.indexOf('.') > 1) {
    var parts = obj.source.split('.', 2);
    obj.source = parts[0];
    obj.subid = parts[1];
  }
  

  //other
  obj.ip = log.remote_addr;
  obj.country = log.geoip_country_code;

  //body
  obj.val = log.request_body || log.http_referer;

  //seclist
  if(obj.seclist) {
    obj.seclist = String.prototype.split.call(obj.seclist, ';')
      .filter(function(i){
        return i.trim();
      })
  }

  if(obj.browserlist) {
    obj.browserlist = String.prototype.split.call(obj.browserlist, ';')
      .filter(function(i){
        return i.trim();
      })
  }

  //rts and cts
  if(obj.rts) {
    obj.rts = new Date(parseInt(obj.rts, 16) * 1000);
  }

  if(obj.cts) {
    obj.cts = new Date(parseInt(obj.cts, 16) * 1000);
  }

  //version
  if(obj.mjv && obj.mnv) {
    obj.osv = obj.mjv + '.' + obj.mnv;
  }

  return obj;
}


module.exports = parseLine;


