
var fs = require('fs');
var split = require('split');
var async = require('async');
var prettyMs = require('pretty-ms');


/**
 *
 * Import single file with given buffer size.
 *
 * @param file {String} file to import.
 * @param filter {Function} Line filter. func(line){} 
 * @param task {Function} Task funciton. func(line, callback) {}
 * @param opt {Object} opt.concurrency default 20; opt.queueSize default 10000;
 * @param done {Function} Callback while finished.
 *
 */
function importFile(file, filter, task, opt, done){
  opt = opt || {};
  opt.concurrency = opt.concurrency || 20;
  opt.queueSize = opt.queueSize || 10000;

  var inputEnd = false;
  var inp = fs.createReadStream(file);
  var timer = null; 
  var stats = {
    sum : 0,
    lines : 0,
    start : new Date().getTime()
  };

  var queue = async.queue(function(line, callback){
    stats.sum++;
    task(line, callback);
  }, opt.concurrency);


  function monitor(){
    stats.end = new Date().getTime();
    stats.spend = (stats.end - stats.start) / 1000;
    stats.lineSpeed = Math.round(stats.lines / stats.spend);
    stats.sumSpeed = Math.round(stats.sum / stats.spend);
    console.log(
      require('util').format('lines:%d, sum:%d, lineSpeed:%d(q/s), sumSpeed:%d(q/s), spend:%s',
        stats.lines, stats.sum, stats.lineSpeed, stats.sumSpeed, prettyMs(stats.spend*1000))
    );
  }

  if(opt.monitor) {
    timer = setInterval(function(){
      monitor();
    }, 1000);
  }

  inp.on('end', function(){
    console.log('input finished.');
    inputEnd = true;
  });

  queue.drain = function(){
    console.log('queue is drain..');
    //resume inp while while queue is drained.
    if(inputEnd) {
      clearInterval(timer);
      monitor();
      done(stats);
    }else{
      inp.isPaused() && inp.resume();
    }
  };

  inp
    .pipe(split())
    .on('data', function(line){
      stats.lines++;
      //filter
      if(filter && filter(line)) return;
      //enqueue
      queue.push(line);
      //pause input while queue is too large.
      if(queue.length() > 10000 && !inp.isPaused()) {
        inp.pause();
      }
    });

  //fix file empty bug.
  setTimeout(function(){
    queue.push('hello');
  }, 500);

}


module.exports = importFile;

