var async = require('async');
var logger = require('./logger');
var Query = require('./query');

var TAG = 'Column';

/*
 * Column obj
 * A column may consists of one or more queries.
 */
function Column(opt, df) {
  if (!opt)
    throw Error('Missing opt');

  if (!df)
    throw Error('Missing column definition');

  this.opt = opt;
  this.df = df;
  this.queries = [];
  this.split();
  this.isDone = false;
  this.result = null;
}

function deepCopy(obj){
  return JSON.parse(JSON.stringify(obj));
}

/*
 *One query will be splited 2 metaquery
 *type: click || conversion
 *dimension: dimension || null
 */
Column.prototype.split = function() {
  var self = this;
  self.df.queries.forEach(function(q){
    var obj = deepCopy(self.opt);
    obj = Object.assign(obj, q);
    //support column filters
    obj.filters = obj.filters || {};
    obj.filters = Object.assign(obj.filters, self.df.filters);
    self.queries.push(new Query(obj));
  });
}


/**
 * Get result
 */
Column.prototype.get = function(done) {
  var self = this;
  async.eachLimit(self.queries , 1, function(query, callback){
    query.get(callback);
  }, function(err){
    if(err) {
      return done && done(err);
    }

    self.isDone = self.queries.every(function(q){
      return q.isDone;
    });

    self.qResult = self.queries.map(function(q){
      return q._result;
    });

    if(self.opt.dimension) {
      self.mergeDimResult();
    }else{
      self.mergeResult();
    }

    done(null, self._result);
  });
}

function mergeKey(obj){
  return Object.keys(obj).map(function(key){
    return obj[key];
  }).join('#');
}

Column.prototype.mergeDimResult = function() {
  var self = this;
  var df = self.df;
  var hash = {};
  var res = [];
  for(var l=0; l<self.qResult.length; l++) {
    res[l] = self.qResult[l].reduce(function(res, cur){
      if(cur.status == 0) {
        cur.data && cur.data.forEach(function(item){
          //console.log(item);
          var key = mergeKey(item._id);
          if(res[key]) res[key] += item.count;
          else res[key] = item.count;
        });
      }
      return res;
    }, {});
  }

  /*
  if(res.length == 1) {
    self._result = {
      status : self.isDone ? 0 : 1,
      data : res[0]
    };
    return ;
  }
  */

  //get all uniq keys.
  var keys = res.reduce(function(keys, cur){
      return keys.concat(Object.keys(cur));
    }, [])
    .sort()
    .reduce(function(arr, cur){
      if(arr.length == 0 || arr[arr.length-1] != cur) {
        arr.push(cur);
      }
      return arr;
    }, []);

  var arr = [];

  for(var i=0; i<keys.length; i++) {
    var o = {};
    o.key = keys[i];
    var dts = [];
    for(var j=0; j<res.length; j++) {
      dts.push(res[j][o.key] || 0);
    }

    if(df.calculate && dts.length > 1) {
      o.data = df.calculate.call(o, dts);
    }else{
      o.data = dts[0];
    }

    arr.push(o);
  }
  self._result = {
    status : (self.isDone ? 0 : 1),
    data : arr
  };

}

Column.prototype.mergeResult = function() {
  var self = this;
  var df = self.df;

  if(df.queries.length == 1) {
    self._result = self.qResult[0].map(function(r){
      var o = {};
      o.status = r.status;
      o.current = r.current;

      if(r.data && r.data.length>0 ) {
        o.data = r.data[0].count;
      }
      return o;
    });
    return ;
  }

  self._result = [];
  var cols = self.qResult.length;
  var rows = self.qResult[0].length;
  for(var i=0; i<rows; i++) {
    self._result[i] = out = {};
    var row = [];
    for(var j=0; j<cols; j++) {
      row.push(self.qResult[j][i]);
    }

    //status
    var sts = row.map(function(q){
      return q.status;
    });
    out.status = Math.max.apply(null, sts);

    var crs = row.map(function(q){
      return q.current;
    });
    out.current = Math.max.apply(null, crs);

    var dts = row.map(function(q){
      if(q.data && q.data.length > 0) {
        return q.data[0].count;
      }
      return 0;
    });

    if(df.calculate) {
      out.data = df.calculate.call(out, dts);
    }
  }
}


/*
 * TODO
 */
Column.prototype.execute = function() {
}

module.exports = Column;
