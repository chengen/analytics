var fs = require('fs');
var async = require('async');
var zlib = require('zlib');
var split = require('split');


function filtering(files, filter, output, done){
  var gzip = zlib.createGzip();
  var output = fs.createWriteStream(output);
  gzip.pipe(output);

  async.eachSeries(files, function(file, callback){
    console.log(file);
    var inp = fs.createReadStream(file);
    inp.pipe(split())
    .on('data', function(line){
      line = line.trim();
      if(line && filter(line)) {
        gzip.write(line + '\n');
      }
    }).on('end', function(){
      callback();
    });
  }, function(){
    gzip.end();
    done();
  });
}

module.exports = filtering;


