var Report = require('./report');



var rpt = {

  title : '留存率',
  columns : [
    {
      name : 'New users',
      queries : [{
        type : 'new',
      }],
    },

    {
      name : '1 day retention',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 1,
        }
      ], 
      calculate : function(r){
        this.data2 = r[1]
        if(r[0] == 0) return 0;
        return r[1] / r[0];
      }
    },

    {
      name : '2 day retention',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 2,
        }
      ], 
      calculate : function(r){
        this.data2 = r[1]
        if(r[0] == 0) return 0;
        return r[1] / r[0];
      }
    },

    {
      name : '3 day retention',
      queries: [
        {
          type : 'new', 
        }, 
        {
          type : 'active',
          evt  : 'visit',
          offset : 3,
        }
      ], 
      calculate : function(r){
        this.data2 = r[1]
        if(r[0] == 0) return 0;
        return r[1] / r[0];
      }
    },


    {
      name : 'Active users',
      queries : [{
        type : 'active',
      }],
    },

    {
      name : 'Events',
      queries : [{
        type : 'event',
      }],
    },
    {
      name : 'active rate',

      queries: [
        {
          type : 'event', 
          evt : 'visit',
        }, 
        {
          type : 'active',
          evt  : 'visit',
        }
      ], 

      calculate : function(r){
        return r[0] / r[1];
      }
    }


  ]
};


var opt = {
  periodType : 'day',
  start: '20160818',
  end: '20160826',
  dimension : ['version'],
  filters : {
    country : 'US'
  }
}



console.log(require('util').inspect(rpt, true, 10));
console.log(require('util').inspect(opt, true, 10));



var report = new Report(opt, rpt);


report.get(function(err, res){
  console.log('isDone', report.isDone);
  if(err) {
    console.log(err);
  }else{
    console.log(require('util').inspect(res, true, 10));
  }
});



