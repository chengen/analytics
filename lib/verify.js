



function verifyUid(uid){
  if(!uid || uid.length < 10) 
    return false;

  var total = 0;
  uid.substring(0, uid.length-2)
    .split('')
    .forEach(function(c){
      var i = c.charCodeAt(0);
      total+=i;
    });

  var c1 = Math.floor(total / (uid.length-2));

  var check = uid.substring(uid.length-2, uid.length);
  var c2 = parseInt(check, 16);
  //console.log(check, c2, c1==c2);
  return c1 == c2;
}

/*
var uid = process.argv[2];


var res = verifyUid(uid);
console.log(uid, res);
*/

module.exports = verifyUid;
