#!/usr/bin/env node

/*
 *
 *
 * Author: chengen@jccy-tech.com
 * 
 * Load log file (gzip or plain text) into mongodb.
 *
 *
 */



var verify = require('../lib/verify');
var User = require('../lib/model_user');
var async = require('async');
var conn = require('../lib/conn');

var queue = async.queue(function(user, callback){
  if(!user.iversion) {
    user.set('iversion', user.version);
    user.save(callback);
  }else{
    callback();
  }
}, 10);

var isDone = false;
console.time('iversion');

queue.drain = function(){
  if(isDone) {
    console.log('All done!');
    console.timeEnd('iversion');
  }
}

User.find({iversion:{$exists:false}}).stream()
  .on('data', function(user){
    queue.push(user);
  })
  .on('error', function(err){
    console.error(err);
  })
  .on('close', function(err){
    isDone = true;
  })

