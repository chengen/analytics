#!/usr/bin/env node

/*
 *
 *
 * Author: chengen@jccy-tech.com
 * 
 * Load log file (gzip or plain text) into mongodb.
 *
 *
 */



var User = require('../lib/model_user');
var async = require('async');
var conn = require('../lib/conn');

var queue = async.queue(function(user, callback){
  if(!user.uuidcheck && user.uuid2) {
  //if( user.uuid2) {

    User.findOne({uuid2:user.uuid2, rtime:{$lt:user.rtime}}, function(err, u){
      user.set('uuidcheck', u ? 'repeat' : 'first');
      console.log(user.uuid2, u ? 'repeat': 'first');
      user.save(callback);
    });

  }else{
    callback();
  }
}, 10);

var isDone = false;
console.time('uuidcheck');

queue.drain = function(){
  if(isDone) {
    console.log('All done!');
    console.timeEnd('uuidcheck');
  }
}

User.find({uuidcheck:{$exists:false}, uuid2:{$exists:true}}).cursor()
//User.find({uuid2:{$exists:true}}).cursor()
  .on('data', function(user){
    queue.push(user);
  })
  .on('error', function(err){
    console.error(err);
  })
  .on('close', function(err){
    isDone = true;
  })

