var fs = require('fs');
var async = require('async');
var zlib = require('zlib');
var split = require('split');
var logfilter = require('./logfilter');
var prettyMs = require('pretty-ms');


var argv = require('optimist')
    .usage('Usage: $0 file1  file2 ...')
    .alias('o', 'output')
    .describe('o', 'output file')
    .demand(['o'])
    .argv;

var files = argv._;

var allgood = files.every(function(f){
  if(fs.existsSync(f)) return true; 
  console.log('ERROR: File', f, 'not exists!');
});


if(!allgood) {
  process.exit(-1);
}


function isVisit(line){
  return ( line.indexOf('1.0.26') > 0 && line.indexOf('evt=visit') > 0 ) ||
    (line.indexOf('1.0.14') > 0 && line.indexOf('/config?') > 0);
}

var start = new Date().getTime(),
    end;

logfilter(files, isVisit, argv.o, function(){
  end = new Date().getTime();
  console.log('all done in', prettyMs(end-start));
});


