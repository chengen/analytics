var mongodb = require('mongodb');
var logger = require('./logger');

// Set up the connection to the local db
var mongoclient = new mongodb.MongoClient();
var opened = false;
var openning = false;
var queue = [];
var db = null;


module.exports.getDb = function(done){
  if(opened){
    process.nextTick(function(){
      done(null, db);
    });
  }else if(openning){
    queue.push(done);
  }else{
    openning = true;
    queue.push(done);

    mongoclient.connect('mongodb://127.0.0.1/analytics', function(err, _db) {
      if(!err)  opened = true;
      db = _db;
      queue.forEach(function(callback){
        process.nextTick(function(){
          callback(err, db);
        });
      });

    });
  }
}


module.exports.close = function(){
  if(opened){
    db.close();
    opened = false;
  }
}
